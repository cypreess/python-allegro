from setuptools import find_packages, setup

setup(
    name='python-allegro',
    description='Python client for Allegro.pl WebAPI',
    long_description='',
    version='2.2',
    packages=['allegro',],
    license='MIT',
    author='Krzysztof Dorosz',
    author_email='cypreess@gmail.com',
    install_requires=['suds-jurko'],
    scripts=('scripts/allegro',)
)