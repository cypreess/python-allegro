# -*- coding: utf8 -*-
from itertools import imap, izip_longest
import logging
import hashlib
import base64
from functools import wraps

import suds
from suds.client import Client


logger = logging.getLogger('allegro_webapi')


def requires_session(f):
    @wraps(f)
    def wrapped(self, *args, **kwargs):
        if self.session is None:
            self._prepare_session()
        try:
            return f(self, *args, **kwargs)
        except suds.WebFault, e:
            if e.fault.faultcode in ('ERR_NO_SESSION', 'ERR_SESSION_EXPIRED'):
                logger.warning(
                    "Session expired, reconnecting with key='%s'" % self.webapi_key)
                self._prepare_session()
                return f(self, *args, **kwargs)
            else:
                raise e

    return wrapped


def wrap_api_error(f):
    @wraps(f)
    def wrapped(self, *args, **kwargs):
        try:
            return f(self, *args, **kwargs)
        except suds.WebFault, e:
            logger.error("Error in %s, key=[%s]\n%s\n%s\n\n" % (
                f.__name__, self.webapi_key, e.fault, e.document))
            if e.fault.faultcode == 'ERR_INPUT_COUNTRY_ERROR':
                raise Allegro.ErrorInputCountry()
            elif e.fault.faultcode == 'ERR_TOO_HIGH_LIMIT_VALUE':
                raise Allegro.ErrorTooHighLimitValue()
            elif e.fault.faultcode == 'ERR_NO_DATABASE':
                raise Allegro.ErrorNoDatabase()
            elif e.fault.faultcode == 'ERR_INVALID_ITEM_ID':
                raise Allegro.ErrorInvalidItemId()
            elif e.fault.faultcode == 'ERR_AUCTION_KILLED':
                raise Allegro.ErrorActionKilled()
            elif e.fault.faultcode in (
                    'ERR_USER_LOGIN_NOT_FOUND', 'ERR_USER_NOT_FOUND'):
                raise Allegro.ErrorUserLoginNotFound()
            elif e.fault.faultcode == 'ERR_WEBAPI_EXPIRED':
                raise Allegro.ErrorWebApiExpired(
                    'WebAPI key: %s' % self.webapi_key)
            elif e.fault.faultcode == 'ERR_WEBAPI_KEY':
                raise Allegro.ErrorWebApiKey(
                    'WebAPI key: %s' % self.webapi_key)
            elif e.fault.faultcode == 'ERR_WEBAPI_KEY_INACTIVE':
                raise Allegro.ErrorWebApiKeyInactive(
                    'WebAPI key: %s' % self.webapi_key)
            elif e.fault.faultcode == 'ERR_WEBAPI_NOT_AVAIL':
                raise Allegro.ErrorWebApiNotAvailable()


            else:
                raise e

    return wrapped


class Allegro:
    """
    Connection handler for Allegro.pl WebAPI
    """
    CURRENCIES = {

        1: u'zł',
        22: u'руб',
        34: u'лв',
        56: u'Kč',
        107: u'тг.',
        168: u'руб',
        181: u'€',
        209: u'грн',


    }
    '''Matches allegro country code to currency symbol'''

    LANG = {
        1: u'PL',
        22: u'BY',
        34: u'BG',
        56: u'CS',  #This is CZ (Czech) lang
        107: u'KZ',
        168: u'RU',
        181: u'SK',
        209: u'UA',

    }
    '''Matches allegro country code to ISO country symbol'''


    class ErrorUserLoginNotFound(Exception):
        pass

    class ErrorInputCountry(Exception):
        pass

    class ErrorTooHighLimitValue(Exception):
        pass

    class ErrorWebApiExpired(Exception):
        pass

    class ErrorWebApiKey(Exception):
        pass

    class ErrorWebApiKeyInactive(Exception):
        pass

    class ErrorWebApiNotAvailable(Exception):
        pass

    class ErrorNoDatabase(Exception):
        pass

    class ErrorInvalidItemId(Exception):
        pass

    class ErrorActionKilled(Exception):
        pass


    _AUCTIONS_LIMIT = 100  # For doGetUserItems
    _AUCTIONS_INFO_LIMIT = 25  # For doGetItemsInfo
    _LIST_AUCTIONS_LIMIT = 1000  # For doGetItemsList

    def __init__(self, webapi_key, country_id,
                 url='https://webapi.allegro.pl/service.php?wsdl', login=None,
                 password_hash=None):
        self.webapi_key = webapi_key
        self.country_id = country_id
        self.session = None
        self.url = url
        self.login = login
        self.password_hash = password_hash
        self.soap_client = Client(self.url)

    def _prepare_session(self):
        logger.info("Preparing session for key='%s'" % self.webapi_key)
        self.doQueryAllSysStatus()
        self.doLoginEnc(self.login, self.password_hash)

    def create_type(self, name):
        return self.soap_client.factory.create(name)

    @wrap_api_error
    def doQueryAllSysStatus(self):
        request = {
            'countryId': self.country_id,
            'webapiKey': self.webapi_key
        }
        response = self.soap_client.service.doQueryAllSysStatus(**request)
        self.systemStatus = {}
        for s in response.item:
            self.systemStatus[s.countryId] = s
        return self.systemStatus

    @wrap_api_error
    def doGetCountries(self):
        request = {
            'countryCode': self.country_id,
            'webapiKey': self.webapi_key
        }
        response = self.soap_client.service.doGetCountries(**request)
        self.countries = {}
        for country in response.item:
            self.countries[country.countryId] = unicode(country.countryName)
        return self.countries

    @wrap_api_error
    def doLoginEnc(self, user_login, user_hash_password):
        request = {
            'userLogin': user_login,
            'userHashPassword': user_hash_password,
            'countryCode': self.country_id,
            'webapiKey': self.webapi_key,
            'localVersion': self.systemStatus[self.country_id].verKey
        }
        response = self.soap_client.service.doLoginEnc(**request)
        self.session = response.sessionHandlePart
        return response


    @wrap_api_error
    @requires_session
    def doGetMyData(self):
        request = {
            'sessionHandle': self.session
        }
        response = self.soap_client.service.doGetMyData(**request)
        return response

    @wrap_api_error
    def doGetUserID(self, user_login, country_id):
        request = {
            'countryId': country_id,
            'userLogin': user_login,
            'webapiKey': self.webapi_key,
        }

        response = self.soap_client.service.doGetUserID(**request)
        return response

    @wrap_api_error
    @requires_session
    def doGetSiteJournal(self, starting=0, info_type=0):
        request = {
            'sessionHandle': self.session,
            'startingPoint': starting,
            'infoType': info_type,
        }

        response = self.soap_client.service.doGetSiteJournal(**request)
        return response


    @wrap_api_error
    @requires_session
    def doGetSiteJournalInfo(self, starting=0, info_type=0):
        request = {
            'sessionHandle': self.session,
            'startingPoint': starting,
            'infoType': info_type,
        }

        response = self.soap_client.service.doGetSiteJournalInfo(**request)
        return response


    @wrap_api_error
    def doGetUserItems(self, user_id, country_id, limit=100, offset=0):
        request = {
            'userId': user_id,
            'webapiKey': self.webapi_key,
            'countryId': country_id,
            'offset': offset,
            'limit': limit,
        }
        response = self.soap_client.service.doGetUserItems(**request)
        return response

    @wrap_api_error
    def doGetUserItemsAll(self, user_id, country_id, limit=100):

        response = self.doGetUserItems(user_id=user_id, country_id=country_id,
                                       offset=0, limit=limit)
        items_count = int(response.userItemCount)
        yield response
        for offset in xrange(items_count / limit):
            response = self.doGetUserItems(user_id=user_id,
                                           country_id=country_id,
                                           offset=offset + 1, limit=limit)
            yield response

    @wrap_api_error
    @requires_session
    def doGetCategoryPath(self, category_id):
        request = {
            'sessionId': self.session,
            'categoryId': category_id,
        }
        return self.soap_client.service.doGetCategoryPath(**request)

    @wrap_api_error
    @requires_session
    def doGetItemsInfo(self, item_id_array, get_desc=False,
                       get_image_url=False, get_attribs=False,
                       get_postage_options=False, get_company_info=False,
                       get_product_info=False):
        request = {
            'sessionHandle': self.session,
            'itemsIdArray': {'item': item_id_array},
            'getDesc': int(get_desc),
            'getImageUrl': int(get_image_url),
            'getAttribs': int(get_attribs),
            'getPostageOptions': int(get_postage_options),
            'getCompanyInfo': int(get_company_info),
            'getProductInfo': int(get_product_info),
        }
        return self.soap_client.service.doGetItemsInfo(**request)

    def doGetItemsInfoAll(self, item_id_array, get_desc=False,
                          get_image_url=False, get_attribs=False,
                          get_postage_options=False, get_company_info=False,
                          get_product_info=False, continue_on_error=False):

        for ids in imap(lambda t: filter(lambda v: v is not None, t),
                        izip_longest(*([iter(
                                item_id_array)] * self._AUCTIONS_INFO_LIMIT))):
            try:
                response = self.doGetItemsInfo(ids, get_desc,
                                               get_image_url, get_attribs,
                                               get_postage_options,
                                               get_company_info,
                                               get_product_info)

                for item in response.arrayItemListInfo.item:
                    yield item
            except:
                if continue_on_error:
                    logger.exception('Webapi error')
                    continue
                else:
                    raise

    @wrap_api_error
    @requires_session
    def doShowItemInfoExt(self, item_id, get_desc=False, get_image_url=False,
                          get_attribs=False, get_postage_options=False,
                          get_company_info=False, get_product_info=False):
        request = {
            'sessionHandle': self.session,
            'itemId': item_id,
            'getDesc': int(get_desc),
            'getImageUrl': int(get_image_url),
            'getAttribs': int(get_attribs),
            'getPostageOptions': int(get_postage_options),
            'getCompanyInfo': int(get_company_info),
            'getProductInfo': int(get_product_info),
        }
        return self.soap_client.service.doShowItemInfoExt(**request)


    # Category tree methods

    @wrap_api_error
    def doGetCatsData(self, country_id):
        request = {
            'countryId': country_id,
            'localVersion': 1,  # value deprecated - not relevant
            'webapiKey': self.webapi_key,
        }
        return self.soap_client.service.doGetCatsData(**request)


    @wrap_api_error
    def doGetCatsDataLimit(self, country_id, offset=0, package_element=5000):
        request = {
            'countryId': country_id,
            'localVersion': 1,  # value deprecated - not relevant
            'webapiKey': self.webapi_key,
            'offset': offset,
            'packageElement': package_element,
        }
        return self.soap_client.service.doGetCatsDataLimit(**request)


    @wrap_api_error
    def doGetItemsList(self, country_id, limit=_LIST_AUCTIONS_LIMIT, offset=0, filter_options=None, sort_options=None, result_scope=None):
        request = {
            'countryId': country_id,
            'webapiKey': self.webapi_key,
            'resultOffset': offset,
            'resultSize': limit,
            'filterOptions': filter_options,
            'sortOptions': sort_options,
            'resultScope': result_scope,
        }
        return self.soap_client.service.doGetItemsList(**request)


    class GET_ITEMS_LIST:
        class SCOPE:
            NO_FILTERS = 1
            NO_CATEGORIES = 2
            NO_ITEMS = 4

    class SEARCH:
        class OPTIONS:
            ALL = 0
            KEYWORDS_OR = 1
            INCLUDE_DESCRIPTION = 2
            ONLY_LAST_24H = 4
            ONLY_BUY_NOW = 8
            INCLUDE_FINISHED = 16
            #FIXME - more flags are documented

        class ORDER_BY:
            FINISH_TIME = 1
            BIDS_COUNT = 2
            PRICE = 4
            TITLE = 8

        class ORDER_TYPE:
            ASC = 0
            DESC = 1

    @wrap_api_error
    @requires_session
    def doSearch(self, query, options=SEARCH.OPTIONS.ALL,
                 order_by=SEARCH.ORDER_BY.FINISH_TIME,
                 order_type=SEARCH.ORDER_TYPE.ASC, limit=100, offset=0):
        request = {
            'sessionHandle': self.session,
            'searchQuery': {
                'searchString': query,
                'searchOptions': options,
                'searchOrder': order_by,
                'searchOrderType': order_type,
                'searchOffset': offset,
                'searchLimit': limit,
            }
        }
        return self.soap_client.service.doSearch(**request)


    @wrap_api_error
    def doSearchAll(self, query, options=SEARCH.OPTIONS.ALL,
                    order_by=SEARCH.ORDER_BY.FINISH_TIME,
                    order_type=SEARCH.ORDER_TYPE.ASC, limit=100):
        response = self.doSearch(query=query, options=options,
                                 order_by=order_by, order_type=order_type,
                                 offset=0, limit=limit)
        items_count = int(response.searchCount)
        if response.searchArray:
            for item in response.searchArray.item:
                yield item
        for offset in xrange(items_count / limit):
            response = self.doSearch(query=query, options=options,
                                     order_by=order_by, order_type=order_type,
                                     offset=offset + 1, limit=limit)
            for item in response.searchArray.item:
                yield item

    class CATEGORY:
        class ORDER:
            FINISH_TIME_ASC = 1
            FINISH_TIME_DESC = 2
            TITLE_ASC = 3
            TITLE_DESC = 4
            PRICE_ASC = 5
            PRICE_DESC = 6
            BIDS_ASC = 7
            BIDS_DESC = 8


    @wrap_api_error
    @requires_session
    def doShowCat(self, cat_id, order=CATEGORY.ORDER.FINISH_TIME_ASC,
                  limit=100, offset=0):

        request = {

            'sessionHandle': self.session,
            'catId': cat_id,
            'catSortOptions': order,
            'catItemsOffset': offset,
            'catItemsLimit': limit,
            'catItemDurationOption': {
                'durationOption': 0,
                'durationValue': 0
            },
            'catItemsPrice': {
                'priceMin': 0.0,
                'priceMax': 0.0
            },
            'catItemState': 0,
            'catOrderFulfillmentTime': 0,
            'catItemOption': 0,


        }

        return self.soap_client.service.doShowCat(**request)


    def doShowCatAll(self, cat_id, order=CATEGORY.ORDER.FINISH_TIME_ASC,
                     limit=100):
        response = self.doShowCat(cat_id, order, limit=limit)
        items_count = int(response.catItemsCount)
        if response.catItemsArray:
            for result in response.catItemsArray.item:
                yield result
        for offset in xrange(items_count / limit):
            response = self.doShowCat(cat_id, order,
                                      offset=offset + 1, limit=limit)
            for item in response.catItemsArray.item:
                yield item

    @staticmethod
    def hash_user_password(password):
        return base64.b64encode(hashlib.sha256(password).digest())
